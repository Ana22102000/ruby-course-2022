user_input = nil
while user_input != 'exit'
  puts "Please insert the value of 'a'"
  user_input = readline.chomp
  a = user_input == 'true'
  b = if a
        'hello'
      elsif a.nil?
        'nil!'
      else
        'foo'
        'baz'
      end
  puts "return val: #{b}"
end

c = case b
    when /hel.*/ then 1
    when 'nil!'  then 5
    when 'baz'   then 10
    else 0
    end
puts "c: #{c}"

10.times do |i|
  puts "iteration #{i}"
end

(3..10).map do |i|
  puts 'foo'
  puts "index: #{i}"
  i * 2
end
