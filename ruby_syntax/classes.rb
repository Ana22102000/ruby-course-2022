class Animal
  def move(distance)
    puts "trying to move... #{distance}"
    return 0 if distance < 0
    distance
  end
end

class Rodent < Animal
end

class Groundhog < Rodent
  attr_reader :age, :weight
  attr_writer :weight
  attr_accessor :name

  def initialize(name = nil, age = 0, weight = 0)
    @name = name
    @age = age
    @weight = weight
    puts "init groundhog... #{name}"
  end

  def move(distance, speed: 1)
    v = super(distance)
    "#{@name} is moving..."
  end
end
